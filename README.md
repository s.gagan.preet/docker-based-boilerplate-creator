# Docker based boilerplate creator

The core purpose of this app is to create single file scripts that can utilise docker to create boilerplates that run on docker. The requirements of building these boilerplates are:

1. Bash
2. Docker

## Boilerplates

Presently no boilerplates exist. All boilerplates will be added in their nascent but functional stages. Over time more features will be added for all scripts.

The core features expected in all these scripts are:

1. Build application boilerplate from a git repository
2. Build Dockerfile
3. Build runApp.sh that enables the user to run the application in developmental mode
4. Build publishApp.sh that enables user to build a publishable version of the application and it's docker image

In addition, an orchestration script will be created that allows the user to create docker-compose files with multiple functioning applications.

### Milestones

1. Python3-Hug API Server
2. React-Redux UI
