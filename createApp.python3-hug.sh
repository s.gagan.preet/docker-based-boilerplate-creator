#!/bin/sh

#
# Boilerplate script to be used for making other scripts in the project
# 


# These variables may be overwritten by the arguments but must always be available
APP_NAME="python3-hug-server"
VERBOSE=true
PORT=5000
FOLDER_PATH=$PWD/$APP_NAME

# These are custom variables that are 

# These variables are constants for an app!
USERNAME="s.gagan.preet"
REPO="python-hug-boilerplate"
REPOSITORY="https://gitlab.com/$USERNAME/$REPO.git"
DOCKER_IMAGE="python3-hug-$APP_NAME"

create_docerfile() {
  if [ $VERBOSE = true ]; then
    echo ">>> Creating Dockerfile"
  fi
  echo "FROM python

MAINTAINER Gagan Preet Singh <gps@gagan-preet.com>

COPY $APP_NAME/requirements.txt /usr/src/app/requirements.txt
RUN pip3 install -r /usr/src/app/requirements.txt

EXPOSE $PORT
WORKDIR /usr/src/app

ENTRYPOINT pip3 freeze > /usr/src/app/requirements.txt && /usr/local/bin/uwsgi --wsgi-file /usr/src/app/server.py --callable __hug_wsgi__ --http 0.0.0.0:$PORT --py-autoreload=3
" > Dockerfile
}

help_message()
{
  echo "This is the createApp script for python3-hug.
  It is a part of the repository: https://gitlab.com/s.gagan.preet/docker-based-boilerplate-creator

  ./createApp.python3-hug.sh
  \t-h --help
  \t\tDisplay this help message
  \t-a | --app
  \t\tApp name - This is used for app name and docker image name
  \t-p | --port
  \t\tPort to expose and run the server on
  \t-v | --verbose
  \t\tVerbose - Print messages at each stage of the createApp process
  "
}

create_app() {
  if [ $VERBOSE = true ]; then
    echo ">>> Creating App by downloading the boilerplate from $REPOSITORY into a folder named $APP_NAME"
  fi
  git clone $REPOSITORY tmp
  cp -r tmp/app $APP_NAME
  rm -r tmp
}

build_docker() {
  if [ $VERBOSE = true ]; then
    echo ">>> Building docker image"
  fi
  docker build -q -t $DOCKER_IMAGE .
}

create_runApp() {
  if [ $VERBOSE = true ]; then
    echo ">>> Creating a runscript for development purposes"
  fi
  echo "#!/bin/bash
docker run -v $FOLDER_PATH:/usr/src/app -p $PORT:$PORT $DOCKER_IMAGE
" > runApp.python3-hug.sh
}

create_prodApp() {
  if [ $VERBOSE = true ]; then
    echo ">>> Creating production App and script"
  fi
}

cleanup() {
  if [ $VERBOSE = true ]; then
    echo ">>> Performing cleanup - Deleting this script"
  fi
  rm createApp.python3-hug.sh
}

run_docker() {
  if [ $VERBOSE = true ]; then
    echo ">>> Running the app"
  fi
  bash runApp.python3-hug.sh
}

process_options() {
  while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
      -h | --help)
        help_message
        exit
        ;;
      -p | --port)
        PORT=$VALUE
        ;;
      -a | --app)
        APP_NAME=$VALUE
        FOLDER_PATH=${pwd}/$APP_NAME
        ;;
      -v | --verbose)
          VERBOSE=true
        ;;
      *)
        echo "ERROR: unknown parameter \"$PARAM\""
        help_message
        exit 1
        ;;
    esac
    shift
  done

}

run_script()
{
  process_options $@
  echo "\tApp Name: $APP_NAME"
  echo "\tPort    : $PORT"
  echo "\tImage   : $DOCKER_IMAGE"
  create_app
  create_docerfile
  build_docker
  create_runApp
  create_prodApp
  cleanup
  run_docker
}

run_script $@
